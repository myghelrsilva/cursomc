package cursomc.miguel.rodrigues.cursom.repositories;

import cursomc.miguel.rodrigues.cursom.domain.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
