package cursomc.miguel.rodrigues.cursom.repositories;

import cursomc.miguel.rodrigues.cursom.domain.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Integer> {

}
