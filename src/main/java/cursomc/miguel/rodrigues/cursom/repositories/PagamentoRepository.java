package cursomc.miguel.rodrigues.cursom.repositories;

import cursomc.miguel.rodrigues.cursom.domain.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentoRepository extends JpaRepository<Pagamento, Integer> {

}
