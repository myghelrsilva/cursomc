package cursomc.miguel.rodrigues.cursom.repositories;

import cursomc.miguel.rodrigues.cursom.domain.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {

}
