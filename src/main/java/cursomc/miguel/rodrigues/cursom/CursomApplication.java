package cursomc.miguel.rodrigues.cursom;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursomApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(CursomApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
