package cursomc.miguel.rodrigues.cursom.services;

import cursomc.miguel.rodrigues.cursom.domain.Cliente;
import cursomc.miguel.rodrigues.cursom.repositories.ClienteRepository;
import cursomc.miguel.rodrigues.cursom.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AuthService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private BCryptPasswordEncoder pe;

    @Autowired
    private EmailService emailService;

    private Random random = new Random();

    public void sendNewPassword(String email){
        Cliente cliente = clienteRepository.findByEmail(email);

        if(cliente == null){
            throw new ObjectNotFoundException("Email não encontrado.");
        }

        String newPass = newPassword();
        cliente.setSenha(pe.encode(newPass));
        clienteRepository.save(cliente);

        emailService.sendNewPasswordEmail(cliente, newPass);

    }

    private String newPassword() {
        char[] vet = new char[10];
        for(int i =0; i< 10; i++){
            vet[i] = randomChar();
        }

        return new String(vet);
    }

    private char randomChar() {
        int opt = random.nextInt(3);
        //Gera um digito
        if(opt == 0){
            return (char) (random.nextInt(10) + 48);
        }
        //Gera letra maiuscula
        else if (opt == 1){
            return (char) (random.nextInt(26)+ 65);
        }
        //letra minuscula
        else{
            return (char) (random.nextInt(26) + 97);
        }
    }
}
