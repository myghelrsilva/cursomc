package cursomc.miguel.rodrigues.cursom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;

public class SmtpMailService extends AbstractEmailService {

    @Autowired
    private MailSender sender;

    @Autowired
    private JavaMailSender mailSender;

    private static final Logger LOG = LoggerFactory.getLogger(SmtpMailService.class);
    @Override
    public void sendEmail(SimpleMailMessage msg) {
        LOG.info("Enviando email.");
        sender.send(msg);
        LOG.info("Email enviado");
    }

    @Override
    public void sendHtmlEmail(MimeMessage msg) {
        LOG.info("Enviando email.");
        mailSender.send(msg);
        LOG.info("Email enviado");
    }
}
