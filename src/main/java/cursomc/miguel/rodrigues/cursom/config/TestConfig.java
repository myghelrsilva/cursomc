package cursomc.miguel.rodrigues.cursom.config;

import cursomc.miguel.rodrigues.cursom.services.DBService;
import cursomc.miguel.rodrigues.cursom.services.EmailService;
import cursomc.miguel.rodrigues.cursom.services.MockEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;

@Configuration
@Profile("test")
public class TestConfig {

    @Autowired
    private DBService dbService;

    @Bean
    public boolean instantiateDataBase() throws ParseException {
        dbService.instantiateTestDatabase();
        return true;
    }

    @Bean
    public EmailService emailService(){
        return new MockEmailService();
    }
}
